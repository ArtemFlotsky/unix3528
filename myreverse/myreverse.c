#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

#define BUF_SIZE 10

void* memmove(void* destination, const void* source, size_t num);

void reverseBuffer(char* buf, int len) {
    int i = 0;
    int m = len / 2;
    for (i = 0; i < m; ++i) {
        char tmp = buf[i];
        buf[i] = buf[len - i - 1];
        buf[len - i - 1] = tmp;
    }
}

int main() {
    char buffer[BUF_SIZE];
    int len = 0;
    
    int s = 0;
    while (1) {
        int n = read(0, buffer + len, BUF_SIZE - len);
        if (n < 0)
            return -1;
        len += n;
        if (len == 0)
            break;
        
        int pos;
        for (pos = 0; pos < len && buffer[pos] != '\n'; ++pos);

        if (pos == len) {
            if (pos == BUF_SIZE) {
                len = 0;
                s = 1;
            } else if (n == 0) {
                break;
            }
        } else {
            if (s) {
                s = 0;
                memmove(buffer, buffer + (pos + 1), len - (pos - 1));
                len -= (pos + 1);
            } else {
                reverseBuffer(buffer, pos);
                int rev_p = 0;
                while (rev_p < pos + 1) {
                    int rev_delta = write(1, buffer + rev_p, pos + 1 - rev_p);
                    if (rev_delta == -1)
                        return -1;
                    rev_p += rev_delta;
                }
                memmove(buffer, buffer + (pos + 1), len - (pos - 1));
                len -= (pos + 1);
            }
        }
    }
    
    return 0;
}