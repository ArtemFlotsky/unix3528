jmp start

GDT:
         dd  0,0
         db  0FFh, 0FFh, 00h, 00h, 01h, 10011010b, 11001111b, 00 ; text  (selector = 8h)
         db  0FFh, 0FFh, 00h, 00h, 01h, 10010010b, 11001111b, 00 ; data  (selector = 10h)
         db  0FFh, 0FFh, 00h, 80h, 0Bh, 10010010b, 01000000b, 00 ; video (selector = 18h)
 
GDT_size = $ - GDT
GDTR :
        dw      GDT_size-1    ; size GDT
        dd      GDT           ; address GDT


; selectors
SEL_DATA        equ   010000b ;10h
SEL_VIDEO       equ   011000b ;18h

;Interrupt Descriptor Table
IDT:
        rw 32
        dw timer_handler,    08h, 1000111000000000b, 0   ; 8 - timer
        dw keyboard_handler, 08h, 1000111000000000b, 0   ; 9 - keyboard
 
IDT_size =  $-IDT
IDTR:
        dw IDT_size-1
        dd IDT

cursor      dd  80  ;cursor position
cursorShift dd   0  ;cursor shift
time        dd   0  ;tics count
buffer      rw 100  ;free space for time calc


start:
;clear screen
        mov     ax,3
        int     10h

;turn on A20
        in      al, 92h
        or      al, 2
        out     92h, al

;turn off interr
        cli

;disable NMI
        in      al, 70h
        or      al, 80h
        out     70h, al  

;load GDT
        xor     eax,eax
        mov     ax, cs
        shl     eax,4
        push    eax
        add     eax, GDT
        mov     dword ptr GDTR+2,eax
        lgdt    fword ptr GDTR

;load IDTR
        xor     eax,eax
        mov     ax, cs
        shl     eax,4
        add     eax, IDT
        mov     dword ptr IDTR+2,eax
        lidt    fword [IDTR]

;go to protected mode
        mov     eax,cr0
        or      al,1
        mov     cr0,eax
        
;load new selector in CS
        jmp     00001000b:PROTECTED_ENTRY

;Protected Mode
use32
PROTECTED_ENTRY:
;init selectors
        mov     ax, SEL_DATA    
        mov     ds, ax
        mov     ss, ax
    
        mov     ax, SEL_VIDEO 
        mov     es, ax

;turn on interr
        in      al, 70h
        and     al, 7Fh
        out     70h, al
        sti

        jmp $
        
;pring number
;   DS:EAX   - number
;   [cursor] - cursor position
;
printNumber:
        pushad
        
; convert int to string and put it to [buffer]
        mov     ebx, 10
        mov     ecx, 1
        xor     edx, edx
calc:
        div     ebx
        mov     dh, 07h
        add     dl, 30h
        mov     [buffer + 2 * ecx], dx
        inc     ecx
        xor     edx, edx
        or      eax, eax
        jnz     calc    
        
        mov     ebx, [cursor]
        dec     ecx
                
print:
        mov     ax, [buffer + 2 * ecx]
        mov     [es:2*ebx], ax
        dec     ecx
        inc     ebx
        or      ecx, ecx
        jnz     print

        popad
        ret


;INTERRUPT HANDLERS

;
; sys timer
timer_handler:
        inc     dword[time]
        mov     eax, [time]
        mov     ebx, [cursor]
        mov     [cursor], 0
        call    printNumber
        mov     [cursor], ebx
        jmp     EOI     

;
;keyboard
keyboard_handler:
        pushad
        xor     ax, ax

;get pos code
        in      al, 060h
        dec     al   
    
;don't process releases
        mov     ah, al
        and     ah, 80h
        jnz     clear_request
        
;print
        call    printNumber
        add     [cursor], 80
        cmp     [cursor], 80*15
        
        jl      @
        mov     [cursor], 80
        add     [cursorShift], 2
        mov     ebx, [cursorShift]
        add     [cursor], ebx
        @:

;send confirmation to keyboard port
        in      al, 061h
        or      al, 80
        out     061h, al
        xor     al, 80
        out     061h, al

clear_request:
        popad
        jmp     EOI

;
; Sends End-Of-Interrupt to controller
EOI:
        push    ax
        mov     al, 20h
        out     020h, al   
        out     0a0h, al   
        pop     ax
        iretd