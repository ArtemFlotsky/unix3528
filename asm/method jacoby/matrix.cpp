#include "matrix.h"
#include "vector_op.h"


        //matrix + matrix
        void Matrix::add_matrix(Matrix b)
        {
                size_t rounded_size = data.size() % 4 * 4;
                add(&this->data[0], &b.data[0], rounded_size);

        for (size_t i = rounded_size; i != data.size() ;++i)
            {
                        this->data[i] += b.data[i];
            }
        }       
        
        //matrix - matrix
        void Matrix::sub_matrix(Matrix b)
        {
                size_t rounded_size = data.size() % 4 * 4;
                sub(&this->data[0], &b.data[0], rounded_size);

        for (size_t i = rounded_size; i != data.size() ;++i)
            {
                        this->data[i] -= b.data[i];
            }
        }       
        
        //matrix * vector
        std::vector < float >  Matrix::mul_vector(std::vector < float > x)
        {
                std::vector < float > result (size, 0);
                std::vector < float > current_row (size);
                
                
                int bonus_zeros = 4 - size % 4;
                
                for(size_t i = 0; i < size; i++)
                {       
                        current_row = this->get_row(i);
                        for(int j = 0; j < bonus_zeros; j++)
                        {
                                current_row.push_back(0);
                        }
                        mul(&current_row[0], &x[0], current_row.size(), &result[i]);
                }
                return result;
        }
        
        //new Matrix = matrix * matrix
        Matrix Matrix::mul_matrix(Matrix b)
        {
                Matrix result(size);
                std::vector < float > current_column (size);
                std::vector < float > result_column (size);

                for(size_t i = 0; i < size; i++)
                {
                        current_column = b.get_column(i);
                        result_column = this->mul_vector(current_column);

                        for(size_t j = i, k = 0; k < size; j += size, k++)
                        {
                                result.data[j]= result_column[k]; 
                        }
                }
                
                return result;
        }
        
        // matrix * const 
        void Matrix::mul_const(float x)
        {
                for(size_t i = 0; i < data.size(); i++)
                {
                        data[i] *= x;
                }
        }
        
        
        size_t Matrix::get_size()
        {
                return this->size;
        }


        float Matrix::at(size_t i, size_t j)
        {
                return data[i * size + j];
        }

        void Matrix::set (float value, size_t i)
        {
                this->data[i] = value;
        }


        void Matrix::print(std::ostream& output)
        {
                for(size_t i = 0; i < size; i++)
                {
                        for(size_t j = 0; j < size; j++)
                        {
                                output << std::setprecision (5) << at(i, j) << " ";
                        }
                        output << std::endl;
                }
        }
        
        
        //use for diagonal matrix only, returns A^-1
        void Matrix::invert()
        {
                for(size_t i = 0; i < size; i++)
                {
                        if (fabs(at(i, i) - (float)0) > EPS) 
                                data[i * size + i] = (1 / at(i, i)); 
                }
        }
        
        std::vector < float > Matrix::get_column(size_t column)
        {
                std::vector < float > result(size, 0);
                for(size_t i = 0; i < size; i++)
                {
                        result[i] = data[column + size * i];
                }
                return result;
        }
        
        std::vector < float > Matrix::get_row(size_t row)
        {
                return std::vector < float >(data.begin() + size * row, data.begin() + size * (row + 1));
        }

        std::vector < float > Matrix::get_diagonal()
        {
                std::vector < float > diagonal (size, 0);
                for(size_t i = 0; i < size; i++)
                {
                        diagonal[i] = at(i, i);
                }
                return diagonal;
        }
        
        //create E * a matrix
        Matrix::Matrix(float a, size_t size)
        {
                this->size = size;
                data.assign(size * size, 0);

                for(size_t i = 0; i < data.size(); i += size + 1)
                {
                        data[i] = a;
                }
        }
        
        //create diagonal matrix with size of diag vector
        Matrix::Matrix(std::vector < float > diag)
        {
                this->size = diag.size();
                data.assign(size * size, 0);

                for(size_t i = 0, j = 0; i < data.size(); i += size + 1, j++)
                {
                        data[i] = diag[j];
                }
        }
        


        //create 0 matrix
        Matrix::Matrix(size_t size)
        {
                this->size = size;
                data.assign(size * size, 0); 
        }
        
        //create matrix from data array
        Matrix::Matrix(std::vector < float > data, size_t size)
        {
                this->data = data;
                this->size = size;
        }
        