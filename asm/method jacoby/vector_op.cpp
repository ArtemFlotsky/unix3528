#include "vector_op.h"

std::vector < float > add(std::vector < float > a, std::vector < float > b)
{       
        size_t rounded_size = a.size() % 4 * 4;
        add(&a[0], &b[0], rounded_size);

    for (size_t i = rounded_size; i != a.size() ;++i)
        {
         a[i] += b[i];
        }
    return a;
}

std::vector < float > sub(std::vector < float > a, std::vector < float > b)
{       
        size_t rounded_size = a.size() % 4 * 4;
    sub(&a[0], &b[0], rounded_size);
        
        for (size_t i = rounded_size; i != a.size() ;++i)
        {
         a[i] -= b[i];
        }
        return a;
}