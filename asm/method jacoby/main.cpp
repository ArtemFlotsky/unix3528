#include "vector_op.h"
#include "matrix.h"
#define BIG_NUM 10000000



int main()
{ 
        std::ifstream input("jacoby.in");
        std::ofstream output("jacoby.out");

        //A*x = b
        
        unsigned int size;
        input >> size; 

        //vector X
        std::vector < float > x (size, 1);
        //vector X for next iteration
        std::vector < float > x_next (size, 0);
        
                
        
        
        //read vector b
        std::vector < float > b (size, 0);
        for(size_t i = 0; i < size; i++)
        {
                input >> b[i];
        }
        
        //read A
        std::vector < float > A_data (size * size, 0);
        for(size_t i = 0; i < A_data.size(); i++)
        {
                input >> A_data[i];
        }
        
        Matrix A (A_data, size);        

        //D = diag(A)
        Matrix D (A.get_diagonal());
                
        //D = D^-1
        D.invert();

        //g = D^-1 * b
        std::vector < float > g (size, 0);
        g = D.mul_vector(b);

        //B = E - D^-1 * A      
        Matrix B (1.0, size);
        B.sub_matrix(D.mul_matrix(A));
        //
        float norm = BIG_NUM;
        int counter = 0;

        while(norm > EPS)
        {
                counter++;

                //x' = B*x + g
                x_next = add(B.mul_vector(x), g);

                std::vector < float > vect_norm (size, 0);
                vect_norm = sub(x, x_next);
                norm = fabs(vect_norm[0]);
                
                for (size_t i = 0; i < size; i++)
                {
                        if (fabs(vect_norm[i]) > norm)
                        {
                                norm = fabs(vect_norm[i]);
                        }
                        x[i] = x_next[i];
                }
                output << "iteration  " << counter << std::endl;
                for(size_t i = 0; i < size; i++)
                        output << x[i] << std::endl;
                output << "norm " << norm << std::endl;

                output << std::endl;


                if(counter > 10000)
                        break;
        }
        
        output << "answer X" << std::endl;
        for(size_t i = 0; i < size; i++)
        {
                output << x[i] << std::endl;
        }

        return 0;
}