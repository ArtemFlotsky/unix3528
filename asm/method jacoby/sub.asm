.model flat
.data?
count   dd      1       
a       dd      1       
b       dd      1       

.code

_sub proc
                push    eax
                
                mov     eax, [esp + 8]
                mov a, eax
                mov     eax, [esp + 12]
                mov b, eax
                mov     eax, [esp + 16]
                mov count, eax

                push    edx
                push    esi
                push    edi

                mov     esi, a
                xor     eax, eax 

start:
                push    eax
                shl     eax, 2
                
                mov     edi, a
                movups  xmm1, [edi + eax]
                mov     edi, b
                movups  xmm2, [edi + eax]

                subps   xmm1, xmm2

                movups  [esi + eax], xmm1

                pop     eax

                add     eax, 4
                cmp     eax, count
                jl      start           
                
                pop     edi
                pop     esi
                pop     edx
                pop     eax

                ret
        
_sub endp
END