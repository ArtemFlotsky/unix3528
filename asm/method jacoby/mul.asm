.model flat
.data?
result  dd      1       
count   dd      1       
a       dd      1       
b       dd      1       

.code

_mul proc
                push    eax
                
                mov     eax, [esp + 8]
                mov a, eax
                mov     eax, [esp + 12]
                mov b, eax
                mov     eax, [esp + 16]
                mov count, eax
                mov     eax, [esp + 20]
                mov result, eax

                push    ecx
                push    edx
                push    esi
                push    edi

                mov     eax, 1
                cpuid

                mov     esi, result

                xorps   xmm0, xmm0              
                xor     eax, eax 

                and     ecx, 00000000000010000000000000000000b
                test    ecx, ecx
                jnz     start_sse4

start_sse3:
                push    eax
                shl     eax, 2
                
                mov     edi, a
                movups  xmm1, [edi + eax]
                mov     edi, b
                movups  xmm2, [edi + eax]
                mulps   xmm1, xmm2
                
                haddps  xmm1, xmm1 
                haddps  xmm1, xmm1

                addps   xmm0, xmm1

                pop     eax

                add     eax, 4
                cmp     eax, count
                jl      start_sse3              
                
                movss   dword ptr [esi], xmm0

                jmp     sse_end

start_sse4:
                push    eax
                shl     eax, 2
                
                mov     edi, a
                movups  xmm1, [edi + eax]
                mov     edi, b
                movups  xmm2, [edi + eax]
                dpps    xmm1, xmm2, 241
;;      241 == 1111 0001  means add(all) and xmm1 = 0 0 0 result
                
                addps   xmm0, xmm1
                pop     eax

                add     eax, 4
                cmp     eax, count
                jl      start_sse4              
                
                movss   dword ptr [esi], xmm0

sse_end:

                pop     edi
                pop     esi
                pop     edx
                pop     ecx
                pop     eax
                ret
        
_mul endp
END