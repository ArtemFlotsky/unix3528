#ifndef _MATRIX_H
    #define _MATRIX_H
        #include <cstdio>
        #include <cstdlib>
    #include <vector>
    #include <iomanip>
    #include <iostream>
    #include <cmath>
    #include <fstream>
    #define  EPS 0.001
        
        class Matrix
        {
        private:
                std::vector < float > data;
                size_t size;
        public:
                size_t get_size();
                std::vector < float > get_column(size_t column);
                std::vector < float > get_row(size_t row);
                std::vector < float > get_diagonal();
                float at(size_t i, size_t j);
                void set(float value, size_t i);
                void print(std::ostream& output);

                void add_matrix(Matrix b);
                void sub_matrix(Matrix b);
                std::vector < float > mul_vector(std::vector < float > x);
                Matrix mul_matrix(Matrix b);
                void mul_const(float x);
                void invert();

                Matrix(float a, size_t size);
                Matrix(unsigned int size);
                Matrix(std::vector < float > diag);
                Matrix(std::vector < float > data, size_t size);
        };



#endif