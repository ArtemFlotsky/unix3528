#ifndef _VECTOR_OP_H
    #define _VECTOR_OP_H
        
    #include <vector>

        extern "C" void mul(float* x, float* y, unsigned int count, float* result);
        extern "C" void add(float* x, float* y, unsigned int count);
        extern "C" void sub(float* x, float* y, unsigned int count);

        std::vector < float > add(std::vector < float > a, std::vector < float > b);
        std::vector < float > sub(std::vector < float > a, std::vector < float > b);

#endif