#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

#define BUF_SIZE 50

int main(){
    static char buf[BUF_SIZE+1];
    int counter = 0;
    /*while(buf != "\n"){
        read(0, buf, 1);
        buf++;
        counter++;
    }*/
    //buf--;
    ssize_t read_bytes = read(STDIN_FILENO,buf,10);
    ssize_t write_bytes = write(STDOUT_FILENO,buf,10);
    printf("%d %d\n",read_bytes, write_bytes);
    /*
    while (counter > 0){
       write(1, buf, 1);
       buf--;
       counter--;
    }
    */
    return (EXIT_SUCCESS);
}