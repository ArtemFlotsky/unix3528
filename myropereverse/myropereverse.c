#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#define BUF_SIZE 10
const char endl = '\n';

typedef struct node node;
struct node {
    node* parent;
    char data[BUF_SIZE];
    int weight;
    int len;
};

void reverseOut (char* buffer, int size) {
    int counter = 0; //written symbols
    
    for (int i = size - 1; i >= 0; --i) {
        if (*(buffer + i) != 0) {
            if (*(buffer + i) != '\n') {                        
                write (1, buffer + i, 1);
            }
            ++counter;
        }
    }
}

node* makeNode (char* data, int weight, int len, node* parent) {
    
    node* new_node = (node*) malloc(sizeof(node));
    
    //checking
    if (weight < 0) weight = 0;    
    new_node->weight = weight;
    
    if (len < 0) len = 0;    
    new_node->len = len;
    
    new_node->parent = parent;
    
    memcpy (&(new_node->data), data, len * sizeof (char));
    
    return new_node;
}


void freeWithPrint (node* nod) {
    
    if (!nod) return;
        
    node* cur_nod = nod;
    //go for
    while (cur_nod) {
        node* old_nod = cur_nod;
        cur_nod = cur_nod->parent;
        reverseOut (old_nod->data, old_nod->len);
        free (old_nod);
    }           
}

void freeNode (node* nod) {
    
    if (!nod) return;

    //test

    node* cur_nod = nod;
    while (cur_nod) {
        node* old_nod = cur_nod;
        cur_nod = cur_nod->parent;
        free (old_nod);
    }           
}



int main() {
    
    char buffer[BUF_SIZE];

    node* tail = 0;

    while (1) {
        int last_read = read (0, buffer, BUF_SIZE);
        if (last_read <= 0) break;

        int beforeEndl;
        int firstEndl = -1;

        for (beforeEndl = 0; beforeEndl < last_read; ++beforeEndl) {
            if (buffer[beforeEndl] == '\n') {
                firstEndl = beforeEndl;
                break;
            }
        }
                
        node* currNode = tail;
        while (currNode != 0) {
            beforeEndl += currNode->len;
            currNode = currNode->parent;
        }

        if (firstEndl < 0) { 
            tail = makeNode (buffer, last_read, last_read, tail);
            continue;
        }

        if (beforeEndl > BUF_SIZE) {
            freeNode (tail);
            tail = 0;
        } else {
            reverseOut (buffer, firstEndl + 1);
            freeWithPrint (tail);
            tail = 0;
            write (1, &endl, 1);
        }

        for (int i = firstEndl + 1; i < last_read; ++i) {
            if (buffer[i] == '\n') {
                reverseOut (buffer + firstEndl + 1, i - firstEndl);
                write (1, &endl, 1);
                firstEndl = i;
            }
        }

        tail = makeNode (buffer + (firstEndl + 1), last_read - (firstEndl + 1), last_read - (firstEndl + 1), tail);
    }

    return 0;
}